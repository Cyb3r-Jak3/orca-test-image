FROM continuumio/miniconda3:latest

RUN apt-get update  > /dev/null && \
    apt-get upgrade -qq  > /dev/null && \
    apt-get install -qq  xvfb libxss1 libgtk2.0-0 libgconf-2-4 libnss3 chromium chromium-l10n curl > /dev/null && \
    conda install -c plotly plotly-orca && \
    conda install -c conda-forge psutil && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

CMD [ "/bin/bash" ]