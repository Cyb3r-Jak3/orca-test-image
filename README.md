# Orca Test

Image used for using orca in a miniconda environment. I wrote it to be used for testing with [MetaStalk](https://gitlab.com/Cyb3r-Jak3/metastalk).


## Using

This project is only avaiable from the GitLab container registry

```bash
docker pull registry.gitlab.com/cyb3r-jak3/orca-test-image/orca-test-image:latest
```
